<?php
/**
 * Created by PhpStorm.
 * User: bartd
 * Date: 22-5-2018
 * Time: 16:41
 */


function get_control_remarks($websiteid, $controlid){

    $remark = \App\WebsiteControl::where('website_id', $websiteid)->where('control_id', $controlid)->first();

    if($remark){
        return $remark->remarks;
    }

    return '';

}