<?php
/**
 * Created by PhpStorm.
 * User: bartd
 * Date: 22-5-2018
 * Time: 16:41
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class WebsiteControl extends Model
{

    public   $timestamps = false;

    protected $table = 'website_control';

}