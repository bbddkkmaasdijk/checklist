<?php
/**
 * Created by PhpStorm.
 * User: bartd
 * Date: 26-3-2018
 * Time: 14:38
 */

namespace App\Http\Controllers;
use App\Http\Requests\SaveUser;
use App\User;

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){

        $users = User::all();

        return view('user.index', compact('users'));
    }

    /**
     * @param SaveUser $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SaveUser $request)
    {
        $user = new User();
        $user->name = $request->get('user');
        $user->save();
        return redirect()->back()->with('message', 'Gebruiker toegevoegd!');
    }

    /**
     * @param SaveUser $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(SaveUser $request, User $user){
        $user->name = $request->get('user');
        $user->save();
        return redirect()->back()->with('message', 'Gebruiker bewerkt!');
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(User $user){
       $user->delete();

        return redirect()->back()->with('message', 'Gebruiker verwijderd!');
    }
}