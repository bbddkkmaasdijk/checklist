<?php
/**
 * Created by PhpStorm.
 * User: bartd
 * Date: 15-5-2018
 * Time: 15:03
 */

namespace App\Http\Controllers;

use App\Http\Requests\SaveWebsite;
use App\User;
use App\Type;
use App\Website;

class AddsiteController extends Controller
{
    public function index(){

        $users = user::orderBy('id', 'desc')->get();
        $types = type::orderBy('id', 'desc')->get();

        return view('addsite.index', compact('users','types'));

    }

    public function store(SaveWebsite $request){
        $website = new website();
        $website->name = $request->get('website');
        $website->user_id = $request->get('user');
        $website->type_id = $request->get('type');
        $website->save();

        return redirect()->back()->with('message', 'website toegevoegd!');
    }
    public function update(){

    }
    public function destroy(){

    }
}