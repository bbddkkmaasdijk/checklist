<?php
/**
 * Created by PhpStorm.
 * User: bartd
 * Date: 16-5-2018
 * Time: 10:02
 */

namespace App\Http\Controllers;

use App\Http\Requests\SaveQuestion;
use App\Question;



class MessageController extends Controller
{
    public function index(){
        return view('message.index');
    }

    public function store(SaveQuestion $request){
        $question = new Question();
        $question->message = $request->get('question');
        $question->save();

        return redirect()->back()->with('message', 'Vraag opgeslagen!');
    }
    public function update(){

       }
    public function destroy( $questionID ){
        $q = Question::find($questionID);
        $q->delete();


        return redirect()->route('home.index')->with('message', 'Vraag beantwoord!');
    }
}