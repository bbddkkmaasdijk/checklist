<?php
/**
 * Created by PhpStorm.
 * User: bartd
 * Date: 26-3-2018
 * Time: 14:38
 */

namespace App\Http\Controllers;


use App\Control;
use App\Website;
use App\WebsiteControl;
use Illuminate\Http\Request;

class EditsiteController extends Controller
{
    public function index($website)
    {
        $website = Website::findOrFail($website);

        $typeid = $website->type->id;

        $controls = Control::where('type_id', $typeid)->get();


        return view('editsite.index', [
            'website' => $website,
            'controls' => $controls
        ]);
    }

    public function update( Request $request, $website )
    {

        $website = Website::findOrFail($website);

        $websitecontrol = \App\WebsiteControl::where('website_id', $website->id)->where('control_id', $request->get('control_id'))->first();

        if(!$websitecontrol) $websitecontrol = new WebsiteControl();

        $websitecontrol->website_id = $website->id;
        $websitecontrol->control_id = $request->get('control_id');
        $websitecontrol->remarks = $request->get('remark');
        $websitecontrol->save();


        return redirect()->back();


    }


    public function destroy()
    {

    }

}
