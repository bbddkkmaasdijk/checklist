<?php
/**
 * Created by PhpStorm.
 * User: bartd
 * Date: 15-5-2018
 * Time: 15:03
 */

namespace App\Http\Controllers;

use App\Control;
use App\Http\Requests\SaveCheck;
use App\Type;
use Illuminate\Foundation\Auth\User;

/**
 * Class ChecklistController
 * @package App\Http\Controllers
 */
class ChecklistController extends Controller
{

    public function index(){

        $controls = Control::orderBy('id', 'desc')->with('type')->get();


        $types = Type::get();


        return view('checklist.index', compact('controls', 'types'));
    }

    public function store(SaveCheck $request)
    {
        $control = new Control();
        $control->name = $request->get('check');
        $control->type_id = $request->get('type');
        $control->save();
        return redirect()->back()->with('message', 'check toegevoegd!!');
    }

    public function update(SaveCheck $request, Control $control){
        $control->name = $request->get('check');
        $control->type_id = $request->get('type');
        $control->save();
        return redirect()->back()->with('message', 'check bwijgewerkt!');
    }
    public function destroy(Control $Control){
        $Control->delete();

        return redirect()->back()->with('message', 'Check verwijderd!');function destroy(){

    }

}}