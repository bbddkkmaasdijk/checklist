<?php
/**
 * Created by PhpStorm.
 * User: bartd
 * Date: 26-3-2018
 * Time: 14:38
 */

namespace App\Http\Controllers;


use App\Question;
use App\Website;
use App\Control;
use App\User;

class DashboardController extends Controller
{
    public function index(){
        $websitecount = Website::count();
        $Controlcount = Control::count();
        $Usercount = User::count();
        $questions = Question::orderBy('id', 'desc')->get();


        return view('home', compact('websitecount', 'Controlcount', 'Usercount', 'questions'));

    }
}
