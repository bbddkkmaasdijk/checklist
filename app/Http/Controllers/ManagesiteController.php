<?php

/**
 * Created by PhpStorm.
 * User: bartd
 * Date: 26-3-2018
 * Time: 14:38
 */

namespace App\Http\Controllers;


use App\Website;

class ManagesiteController extends Controller
{
    public function index(){
        $websites = Website::all();
        return view('managesite.index', compact('websites'));
    }

    public function store(){

    }
    public function update(){

    }
    public function destroy(){

    }
}