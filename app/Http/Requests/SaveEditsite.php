<?php
/**
 * Created by PhpStorm.
 * User: bartd
 * Date: 22-5-2018
 * Time: 15:26
 */
namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class SaveEditsite extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
}
