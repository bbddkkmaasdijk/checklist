<?php
/**
 * Created by PhpStorm.
 * User: bartd
 * Date: 18-5-2018
 * Time: 12:18
 */

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class SaveUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user'=> 'required',
        ];
    }
}
