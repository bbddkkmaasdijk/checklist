<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Control extends Model
{
    protected $table = 'control';
    public $timestamps = false;

    public function type()
    {
        return $this->belongsTo(Type::class);
    }

}
