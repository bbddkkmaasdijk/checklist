<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Index
Route::get('/questions/destroy/{question}',  ['uses' => 'DashboardController@destroy', 'as' => 'message.destroy']);
Route::get('/', ['uses' => 'DashboardController@index', 'as' => 'home.index']);
Route::get('/users',  ['uses' => 'UserController@index', 'as' => 'user.index']);
Route::get('/users/{user}',  ['uses' => 'UserController@edit', 'as' => 'user.edit']);
//Website toevoegen
Route::get('/addsite',  ['uses' => 'AddsiteController@index', 'as' => 'addsite.index']);
Route::post('/website/store',  ['uses' => 'AddsiteController@store', 'as' => 'website.store']);
Route::get('/addsite/destroy/{checklist}',  ['uses' => 'AddsiteController@destroy', 'as' => 'addsite.destroy']);
Route::post('/addsite/update',  ['uses' => 'AddsiteController@update', 'as' => 'addsite.update']);
//Website beheren
Route::get('/managesite',  ['uses' => 'ManagesiteController@index', 'as' => 'managesite.index']);
Route::post('/managesite/store',  ['uses' => 'ManagesiteController@store', 'as' => 'managesite.store']);
Route::get('/managesite/destroy/{checklist}',  ['uses' => 'ManagesiteController@destroy', 'as' => 'managesite.destroy']);
Route::post('/managesite/update',  ['uses' => 'ManagesiteController@update', 'as' => 'managesite.update']);
//Checklist beheren
Route::get('/checklist',  ['uses' => 'ChecklistController@index', 'as' => 'checklist.index']);
Route::post('/checklist/store',  ['uses' => 'ChecklistController@store', 'as' => 'checklist.store']);
Route::get('/checklist/destroy/{control}',  ['uses' => 'ChecklistController@destroy', 'as' => 'checklist.destroy']);
Route::post('/checklist/update{control}',  ['uses' => 'ChecklistController@update', 'as' => 'checklist.update']);
//Gebruikers beheren
Route::get('/user',  ['uses' => 'UserController@index', 'as' => 'user.index']);
Route::post('/user/store',  ['uses' => 'UserController@store', 'as' => 'user.store']);
Route::get('/user/destroy/{user}',  ['uses' => 'UserController@destroy', 'as' => 'user.destroy']);
Route::post('/user/update/{user}',  ['uses' => 'UserController@update', 'as' => 'user.update']);
//Vraag stellen
Route::get('/message',  ['uses' => 'MessageController@index', 'as' => 'message.index']);
Route::post('/message/store',  ['uses' => 'MessageController@store', 'as' => 'message.store']);
Route::get('/message/destroy/{checklist}',  ['uses' => 'MessageController@destroy', 'as' => 'message.destroy']);
Route::post('/message/update',  ['uses' => 'MessageController@update', 'as' => 'message.update']);
// website beheren
Route::get('/editsite/{site}',  ['uses' => 'EditsiteController@index', 'as' => 'editsite.index']);
Route::post('/editsite/update/{site}',  ['uses' => 'EditsiteController@update', 'as' => 'editsite.update']);