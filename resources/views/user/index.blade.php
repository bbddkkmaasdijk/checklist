@extends('master')
@section('title', 'Gebruikers')
@section('content')

    <div class='col-md-12'>
        <div class="card">
            <div class="header">
                <h4 class="title"><strong>Gebruiker</strong> bewerken</h4>
            </div>
            <div class="content">

                <?php foreach($users as $user):?>
                <form method="POST" action="{{ route('user.update', $user->id) }}">
                    <div class="row">
                        <div class="col-md-1">
                            <div class="form-group">
                                <input type="text" class="form-control border-input" value="<?php echo $user->id?>" disabled placeholder="1" value="1">
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">

                                <input type="text" name="user" class="form-control border-input" value="<?php  echo $user->name?>">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <button class='btn btn-info btn-fill btn-wd'>Bewerken</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <a class='btn btn-info btn-fill btn-wd' href="{{ route('user.destroy', $user->id) }}">Verwijderen</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <?php  endforeach;?>

            </div>

            <div class="clearfix"></div>

        </div>
    </div>
    <div class='col-md-12'>
        <div class="card">
            <div class="header">
                <h4 class="title">Gebruikers toevoegen</h4>
            </div>
            <div class="content">
                <form method="post" action="{{ route('user.store') }}">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Naam</label>
                                <input type="text" class="form-control border-input" placeholder="Gebruikersnaam" name="user">
                                <input type="hidden" name="action" value="add_user" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-11">
                        <div class="form-group">
                            <button type='submit' class='btn btn-info btn-fill btn-wd'>Gebruiker toevoegen</button>
                        </div>
                    </div>

            <div class="clearfix"></div>
            </form>
        </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>

@stop