@extends('master')
@section('title', 'Vraag stellen')
@section('content')
    <div class="content">
        <div>
            <div class="row">
            </div>
            <div class='col-md-12'>
                <div class="card">
                    <div class="header">
                        <h4 class="title">Vraag stellen</h4>
                    </div>
                    <div class="content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Vraag:</label>
                                    <form method="POST" action="{{ route('message.store') }}">
                                        <input type="text" class="form-control border-input" placeholder="Vraag" name="question">
                                        <div class="col-md-3">
                                            <button type='submit' class='btn btn-info btn-fill btn-wd'>Vraag stellen</button>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="clearfix"></div>

                </div>
            </div>
        </div>
    </div>
    </div>
    </div>

@stop