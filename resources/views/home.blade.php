@extends('master')
@section('title', 'Dasbboard')
@section('content')
    <div class='content'>
        <div class='container-fluid'>
            <div class='row'>
                <div class='col-lg-3 col-sm-6'>
                    <div class='card'>
                        <div class='content'>
                            <div class='row'>
                                <div class='col-xs-5'>
                                    <div class='icon-big icon-warning text-center'>
                                        <i class='ti-dashboard'></i>
                                    </div>
                                </div>
                                <div class='col-xs-7'>
                                    <div class='numbers'>
                                        <p>Aantal websites</p>
                                        <?php echo $websitecount;?>
                                    </div>
                                </div>
                            </div>
                            <div class='footer'>
                                <hr />
                                <div class='stats'>
                                    <i class='ti-eye'></i> <a href='{{ route('managesite.index') }}'>Bekijken</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='col-lg-3 col-sm-6'>
                    <div class='card'>
                        <div class='content'>
                            <div class='row'>
                                <div class='col-xs-5'>
                                    <div class='icon-big icon-success text-center'>
                                        <i class='ti-check'></i>
                                    </div>
                                </div>
                                <div class='col-xs-7'>
                                    <div class='numbers'>
                                        <p>Checks</p>
                                        <?php echo $Controlcount;?>
                                    </div>
                                </div>
                            </div>
                            <div class='footer'>
                                <hr />
                                <div class='stats'>
                                    <i class='ti-eye'></i>  <a href='{{ route('checklist.index') }}'>Bekijken</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='col-lg-3 col-sm-6'>
                    <div class='card'>
                        <div class='content'>
                            <div class='row'>
                                <div class='col-xs-5'>
                                    <div class='icon-big icon-danger text-center'>
                                        <i class='ti-user'></i>
                                    </div>
                                </div>
                                <div class='col-xs-7'>
                                    <div class='numbers'>
                                        <p>Gebruikers</p>
                                        <?php echo $Usercount;?>
                                    </div>
                                </div>
                            </div>
                            <div class='footer'>
                                <hr />
                                <div class='stats'>
                                    <i class='ti-eye'></i> <a href='{{ route('user.index') }}'>Bekijken</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='col-lg-3 col-sm-6'>
                    <div class='card'>
                        <div class='content'>
                            <div class='row'>
                                <div class='col-xs-5'>
                                    <div class='icon-big icon-info text-center'>
                                        <i class='ti-calendar'></i>
                                    </div>
                                </div>
                                <div class='col-xs-7'>
                                    <div class='numbers'>
                                        <p>Week</p>
                                        <?php echo $currentWeekNumber = date('W');?>
                                    </div>
                                </div>
                            </div>
                            <div class='footer'>
                                <hr />
                                <div class='stats'>
                                    <i class='ti-alarm-clock'></i> <a><?php  echo "Het is vandaag " . date("d-m-y")?> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class='content'>
            <div class='container-fluid'>
                <div class='row'>
                    <div class='col-md-12'>
                        <div class='card'>
                            <div class='header'>
                                <h4 class='title'>Vragen</h4>
                                <p class='category'>Stel hier je vragen over de checklist</p>
                            </div>
                            <div class='content table-responsive table-full-width'>
                                <table class='table table-striped'>
                                    <thead>
                                    <th>Naam</th>
                                    <th>Vraag</th>
                                    <th>beantwoord</th>
                                    </thead>
                                    <tbody>
                                    <?php foreach($questions as $question):?>
                                    <form method="post">
                                        <tr>
                                            <td><?php echo $question->id?></td>
                                            <td><?php echo $question->message?></td>
                                            <input type="hidden" name="action" value="delite_message" />
                                            <input type="hidden" name="id" value="<?php //echo $Message->id ?>"/>
                                            <td>

                                                <a href="{{ route('message.destroy', $question) }}">Vraag beantwoord</a></td>

                                    </form>
                                    </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop