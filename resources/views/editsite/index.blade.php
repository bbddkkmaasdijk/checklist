@extends('master')
@section('title', 'Website beheren')
@section('content')
        <div>
            <div class="row">
            </div>
            <div class='col-md-12'>
                <div class="card">
                    <div class="header">
                        <h4 class="title">{{ $website->name }} beheren</h4>
                    </div>
                    <div class="content">
                        @foreach($controls as $control)
                            <?php $remark = get_control_remarks($website->id, $control->id)?>
                        <form method="post" action="{{ route('editsite.update', $website->id) }}">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>check:</label>
                                        <input type="text" class="form-control border-input" value="{{ $control->name }}" disabled>
                                    </div>
                                </div>
                                <input type="hidden" name="control_id" value="{{$control->id}}" />
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>eventueele opmerking:</label>
                                        <input type="text" name="remark" class="form-control border-input" value="{{ $remark }}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        @if($remark)
                                        <label class="spacelabel"></label>
                                        <button class='btn btn-success btn-fill btn-wd'>Updaten</button>

                                            @else
                                            <label class="spacelabel"></label>
                                            <button class='btn btn-info btn-fill btn-wd done'>Afgerond</button>

                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                            @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@stop