@extends('master')
@section('title', 'Website toevoegen')
@section('content')
    <div class="content">
        <div>
            <div class="row">
            </div>
            <div class='col-md-12'>
                <div class="card">
                    <div class="header">
                        <h4 class="title">Wesbite toevoegen</h4>
                    </div>
                    <div class="content">
                        <form method="POST" action="{{ route('website.store') }}">
                            <div class="row">

                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label>Wesbite:</label>
                                        <input type="text" name="website" class="form-control border-input" placeholder="Website">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Beheerder:</label>
                                        <select name="user" class="form-control border-input">
                                            <?php foreach($users as $user):?>
                                            <option value="<?php  echo $user->id?>">  <?php  echo $user->name?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Pakket:</label>
                                        <select name="type" class="form-control border-input">
                                            <?php foreach($types as $type):?>
                                            <option value="<?php echo $type->id?>">  <?php echo $type->name?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <button type='submit' class='btn btn-info btn-fill btn-wd'>Wesbite toevoegen</button>
                                </div>
                            </div>
                    <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop