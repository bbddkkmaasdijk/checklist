
<!doctype html>
<html lang='nl'>

<head>
    <meta charset='utf-8' />
    <link rel='apple-touch-icon' sizes='76x76' href='assets/img/apple-icon.png'>
    <link rel='icon' type='image/png' sizes='96x96' href='assets/img/favicon.png'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />

    <title>Toscani Checklist</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name='viewport' content='width=device-width' />
  <base href="/" />

    <!-- Bootstrap core CSS     -->
    <link href='assets/css/bootstrap.min.css' rel='stylesheet' />

    <!-- Animation library for notifications   -->
    <link href='assets/css/animate.min.css' rel='stylesheet' />

    <!--  Paper Dashboard core CSS    -->
    <link href='assets/css/paper-dashboard.css' rel='stylesheet' />


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href='assets/css/demo.css' rel='stylesheet' />


    <!--  Fonts and icons     -->
    <link href='http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href='assets/css/themify-icons.css' rel='stylesheet'>

</head>

<body>

<div class='wrapper'>
    <div class='sidebar' data-background-color='white' data-active-color='danger'>

        <!--
        Tip 1: you can change the color of the sidebar's background using: data-background-color='white | black'
        Tip 2: you can change the color of the active button using the data-active-color='primary | info | success | warning | danger'
    -->

        <div class='sidebar-wrapper'>
            <div class='logo'>
                <a href='http://toscani.nl' class='simple-text'>
                    Toscani checklist
                </a>
            </div>

            <ul class='nav'>
                <li class='active'>
                    <a href='/'>
                        <i class='ti-panel'></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li>
                <li>
                    <a href='{{ route('addsite.index') }}'>
                        <i class='ti-plus'></i>
                        <p>Website toevoegen</p>
                    </a>
                </li>
                <li>
                    <a href='{{ route('managesite.index') }}'>
                        <i class='ti-search'></i>
                        <p>Website beheren</p>
                    </a>
                </li>
                <li>
                    <a href='{{ route('checklist.index') }}'>
                        <i class='ti-menu-alt'></i>
                        <p>Checklist beheren</p>
                    </a>
                </li>
                <li>
                    <a href='{{ route('user.index') }}'>
                        <i class='ti-user'></i>
                        <p>Gebruikers</p>
                    </a>
                </li>
                <li>
                    <a href='{{ route('message.index') }}'>
                        <i class='ti-marker'></i>
                        <p>Vraag stellen</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class='main-panel'>
        <nav class='navbar navbar-default'>
            <div class='container-fluid'>
                <div class='navbar-header'>
                    <button type='button' class='navbar-toggle'>
                        <span class='sr-only'>Toggle navigation</span>
                        <span class='icon-bar bar1'></span>
                        <span class='icon-bar bar2'></span>
                        <span class='icon-bar bar3'></span>
                    </button>
                    <a class='navbar-brand' href='#'>@yield('title')</a>
                </div>
                <!--                later keuze uit gebruikers inbouwen eventueel-->
                <!--                <div class='collapse navbar-collapse'>-->
                <!--                    <ul class='nav navbar-nav navbar-right'>-->
                <!--                        <li class='dropdown'>-->
                <!--                            <a href='#' class='dropdown-toggle' data-toggle='dropdown'>-->
                <!--                                <i class='ti-user'></i>-->
                <!---->
                <!--                                <p>bart</p>-->
                <!--                                <b class='caret'></b>-->
                <!---->
                <!--                            </a>-->
                <!--                            <ul class='dropdown-menu'>-->
                <!---->
                <!--                                --><?php //foreach($users as $user):?>
                        <!--                                <li><a href='#'>--><?php // echo $user->name?><!--</a></li>-->
                <!--                                --><?php //endforeach;?>
                        <!--                            </ul>-->
                <!---->
                <!--                        </li>-->
                <!--                    </ul>-->
                <!---->
                <!--                </div>-->
            </div>
        </nav>


        <div class='content'>
            <div class='container-fluid'>
                <div class='row'>

                    @if(Session::has('message'))

                        <div class="alert alert-success">

                            {{ Session::get('message') }}

                            </div>

                    @endif

                    @if(!$errors->isEmpty())

                        <div class="alert alert-danger">

                            <ul>
                                @foreach($errors->all() as $error)

                                    <li>{{ $error }}</li>

                                @endforeach
                            </ul>

                            </div>


                        @endif

                    @yield('content')

                </div>
            </div>
        </div>



        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>
                        <li>
                            <a href="http://toscani.nl">
                                Toscani
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script>, made with <i class="fa fa-heart heart"></i> by <a href="http://toscani.nl">Bart de Koning</a>
                </div>
            </div>
        </footer>
        <!--   Core JS Files   -->
        <script src='assets/js/jquery-1.10.2.js' type='text/javascript'></script>
        <script src='assets/js/bootstrap.min.js' type='text/javascript'></script>

        <!--  Checkbox, Radio & Switch Plugins -->
        <script src='assets/js/bootstrap-checkbox-radio.js'></script>

        <!--  Charts Plugin -->
        <script src='assets/js/chartist.min.js'></script>

        <!--  Notifications Plugin    -->
        <script src='assets/js/bootstrap-notify.js'></script>

        <!--  Google Maps Plugin    -->
        <script type='text/javascript' src='https://maps.googleapis.com/maps/api/js'></script>

        <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
        <script src='assets/js/paper-dashboard.js'></script>

        <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
        <script src='assets/js/demo.js'></script>
        <!--Eventueel alert instellen-->
        <!--<script type='text/javascript'>-->
        <!--$(document).ready(function(){-->

        <!--demo.initChartist();-->

        <!--$.notify({-->
        <!--message: 'Welkom bij de <b>Toscani Checklist</b>'-->

        <!--},{-->
        <!--type: 'success',-->
        <!--timer: 4000-->
        <!--});-->

        <!--});-->
        <!--</script>-->

</body>
</html>