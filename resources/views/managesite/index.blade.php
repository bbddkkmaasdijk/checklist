@extends('master')
@section('title', 'Website beheren')
@section('content')
    <div class="content">
        <div>
            <div class="row">
            </div>
            <div class='col-md-12'>
                <div class="card">
                    <div class="header">
                        <h4 class="title">Wesbite beheren</h4>
                    </div>
                    <div class="content">
                        <form>
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Wesbite beheren:</label>

                                        <table>
                                            @foreach($websites as $website)
                                                <?php if(!empty($website->user)):?>
                                                <tr>
                                                    <Td><a href="{{ route('editsite.index', [$website->id]) }}"><?php  echo $website->name?> - <?php  echo $website->type->description?> - <?php echo $website->User->name ?></a></Td>
                                                </tr>
                                                <?php endif;?>


                                                @endforeach
                                        </table>

                                    </div>

                                </div>
                            </div>

                    </div>

                    <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>


    </div>
    </div>
    </div>
@stop