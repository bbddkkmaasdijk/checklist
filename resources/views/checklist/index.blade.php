@extends('master')
@section('title', 'Checklist beheren')
@section('content')
    <div class="content">
        <div>
            <div class="row">
            </div>
            <div class='col-md-12'>
                <div class="card">
                    <div class="header">
                        <h4 class="title">Checklist bewerken</h4>
                    </div>
                    <div class="content">
                        <?php foreach($controls as $control):?>
                        <form method="POST" action="{{ route('checklist.update', $control->id) }}">
                            <div class="row">
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control border-input" value="<?php echo $control->id?>" disabled placeholder="1" value="1">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">

                                        <input type="text" name="check" class="form-control border-input" value="<?php echo $control->name?>">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <select name="type" class="form-control border-input" value="3">

                                            <?php foreach($types as $type):?>
                                            <option value="<?php  echo $type->id?>" @if($type->id == $control->type->id) selected @endif>  <?php  echo $type->name?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <button class='btn btn-info btn-fill btn-wd'>Bewerken</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <a class='btn btn-info btn-fill btn-wd' href="{{ route('checklist.destroy', $control->id) }}">Verwijderen</a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <?php endforeach;?>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class='col-md-12'>
                <div class="card">
                    <div class="header">
                        <h4 class="title">Check toevoegen</h4>
                    </div>
                    <div class="content">
                        <form method="post" action="{{ route('checklist.store') }}">
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label>Check</label>
                                        <input type="text" class="form-control border-input" placeholder="Check" name="check">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Pakket:</label>
                                        <select name="type" class="form-control border-input">
                                            <?php foreach($types as $type):?>
                                            <option value="<?php echo $type->id?>">  <?php  echo $type->name?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-11">
                                <div class="form-group">
                                    <button type='submit' class='btn btn-info btn-fill btn-wd'>toevoegen</button>
                                </div>
                            </div>
                    <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
@stop